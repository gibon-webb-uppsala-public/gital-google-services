��          �       �      �     �     �     �     �     �  &   
     1     M     e     w     �  y   �  *   "     M     ]  i   m  ;   �  5     2   I  I   |  2   �  <   �  �  6  #   �     �  '        3     S  &   m     �     �     �     �       �     4   �     �     �  }   
	  =   �	  ?   �	  F   
  F   M
  V   �
  F   �
   Activate Downloads Tracking Activate E-Mail Tracking Activate Film Tracking Activate Form Tracking Activate Google Analytics Activate Google Analytics Consent Mode Activate Google Tag Manager Activate Phone Tracking Additional script Disabled for security reasons Documents to track Enter the settings for Google Analytics. For form tracking to work, please activate Google Analytics through this plugin. Enter the settings for Google Tag Manager. Google Services Second  gtag ID This option lets you add document types that should be considered as downloads. Separate them with comma. This option will activate the Google Analytics Consent Mode This option will track the clicks on "mailto:" links. This option will track the clicks on "tel:" links. This option will track the clicks on links with the "download" attribute. This option will track the submits of Ninja Forms. This option will track when the visitor plays video elements Project-Id-Version: Gital Google Services
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-08-24 14:43+0000
PO-Revision-Date: 2023-11-13 10:22+0000
Last-Translator: 
Language-Team: Svenska
Language: sv_SE
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.2; wp-5.5 Aktivera spårning av nedladdnignar Aktivera spårning av e-post Aktivera spårning av filmuppspelningar Aktivera spårning av formulär Aktivera Google Analytics Aktivera Google Analytics Consent Mode Aktivera Google Tag Manager Aktivera telefonnummerspårning Ytterligare skript Inaktiverat av säkerhetsskäl Dokument att spåra Lägg till inställningarna för Google Analytics. För att formulärspårning skall fungera korrekt, var god aktivera Google Analytics via detta plugin. Lägg till inställningarna för Google Tag Manager. Google Tjänster Ytterligare gtag ID Med det här alternativet kan du lägga till dokumenttyper som ska betraktas som nedladdningar. Separera dem med kommatecken. Det här alternativet aktiverar Google Analytics Consent Mode Detta alternativ kommer att spåra klick på "mailto:" länkar. Det här alternativet aktiverar spårning av klick på "tel:" länkar. Detta alternativ spårar klick på länkar med attributet "ladda ner". Det här alternativet aktiverar spårning av inlämnade formulär från "Ninja Forms". Detta alternativ kommer att spåra när besökaren spelar videoelement 