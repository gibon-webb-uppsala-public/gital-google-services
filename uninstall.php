<?php
/**
 * Uninstall routine
 *
 */

if (! defined('WP_UNINSTALL_PLUGIN')) {
    exit();
}

// Delete the options.
delete_option('g_gs_settings');
delete_site_option('g_gs_settings');