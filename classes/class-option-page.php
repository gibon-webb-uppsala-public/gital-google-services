<?php
/**
 * Option page
 *
 * @package Gital Google Services
 */

namespace gital_google_services;

if ( ! class_exists( 'Option_Page' ) ) {
	/**
	 * Option_Page
	 *
	 * Sets up the option page with it's fields
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 * @version 1.3.0
	 * @since 1.2.0
	 */
	class Option_Page {
		/**
		 * The options
		 *
		 * @var array
		 */
		public $options;

		public function __construct() {
			$this->options = get_option( 'g_gs_settings' );
			add_action( 'admin_menu', array( $this, 'option_page' ) );
			add_action( 'admin_init', array( $this, 'settings_init' ) );
		}

		/**
		 * Adds the options page to manage the settings
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.2.0
		 */
		public function option_page() {
			add_options_page( __( 'Google Services', 'gital-gs' ), __( 'Google Services', 'gital-gs' ), 'administrator', 'g-gs-settings', array( $this, 'option_page_render' ) );
		}

		/**
		 * Renders the form
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.2.0
		 */
		public function option_page_render() {
			echo '<form action="options.php" method="post">';
			echo '<h1>' . __( 'Google Services', 'gital-gs' ) . '</h1>';
			settings_fields( 'g_gs' );
			do_settings_sections( 'g_gs' );
			submit_button();
			echo '</form>';
		}

		/**
		 * Renders the option page description for Google Analytics
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.2.0
		 */
		public function settings_section_ga_description() {
			echo '<p>' . esc_html__( 'Enter the settings for Google Analytics. For form tracking to work, please activate Google Analytics through this plugin.', 'gital-gs' ) . '</p>';
		}

		/**
		 * Renders the option page description for Google Tag Manager
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.2.0
		 */
		public function settings_section_gtm_description() {
			echo '<p>' . esc_html__( 'Enter the settings for Google Tag Manager.', 'gital-gs' ) . '</p>';
		}

		/**
		 * Renders the "Activate Google Analytics" checkbox
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.2.0
		 */
		public function settings_activate_ga_render() {
			echo '<input type="checkbox" name="g_gs_settings[activate_ga]" value="1"' . checked( 1, $this->options['activate_ga'] ?? '', false ) . '>';
		}

		/**
		 * Renders the "Activate Google Tag Manager" checkbox
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.2.0
		 */
		public function settings_activate_gtm_render() {
			echo '<input type="checkbox" name="g_gs_settings[activate_gtm]" value="1"' . checked( 1, $this->options['activate_gtm'] ?? '', false ) . '>';
		}

		/**
		 * Renders the "Activate Form Tracking" checkbox
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 * @since 1.2.0
		 */
		public function settings_activate_form_tracking_render() {
			echo '<input type="checkbox" name="g_gs_settings[activate_form_tracking]" value="1"' . checked( 1, $this->options['activate_form_tracking'] ?? '', false ) . '>';
			echo '<p><i>' . __( 'This option will track the submits of Ninja Forms.', 'gital-gs' ) . '</i></p>';
		}

		/**
		 * Renders the "Activate Phone Tracking" checkbox
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.3.0
		 */
		public function settings_activate_phone_tracking_render() {
			echo '<input type="checkbox" name="g_gs_settings[activate_phone_tracking]" value="1"' . checked( 1, $this->options['activate_phone_tracking'] ?? '', false ) . '>';
			echo '<p><i>' . __( 'This option will track the clicks on "tel:" links.', 'gital-gs' ) . '</i></p>';
		}

		/**
		 * Renders the "Activate E-Mail Tracking" checkbox
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 2.0.0
		 */
		public function settings_activate_mail_tracking_render() {
			echo '<input type="checkbox" name="g_gs_settings[activate_mail_tracking]" value="1"' . checked( 1, $this->options['activate_mail_tracking'] ?? '', false ) . '>';
			echo '<p><i>' . __( 'This option will track the clicks on "mailto:" links.', 'gital-gs' ) . '</i></p>';
		}

		/**
		 * Renders the "Activate Film tracking" checkbox
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 2.0.0
		 */
		public function settings_activate_film_tracking_render() {
			echo '<input type="checkbox" name="g_gs_settings[activate_film_tracking]" value="1"' . checked( 1, $this->options['activate_film_tracking'] ?? '', false ) . '>';
			echo '<p><i>' . __( 'This option will track when the visitor plays video elements', 'gital-gs' ) . '</i></p>';
		}

		/**
		 * Renders the "Activate Downloads Tracking" checkbox
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 2.0.0
		 */
		public function settings_activate_downloads_tracking_render() {
			echo '<input type="checkbox" name="g_gs_settings[activate_downloads_tracking]" value="1"' . checked( 1, $this->options['activate_downloads_tracking'] ?? '', false ) . '>';
			echo '<p><i>' . __( 'This option will track the clicks on links with the "download" attribute.', 'gital-gs' ) . '</i></p>';
		}

		/**
		 * Renders the "Documents to track" checkbox
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 2.0.0
		 */
		public function settings_downloads_tracking_documents_render() {
			echo '<input placeholder="pdf,zip" type="text" name="g_gs_settings[downloads_tracking_documents]" value="' . esc_js( $this->options['downloads_tracking_documents'] ?? '' ) . '">';
			echo '<p><i>' . __( 'This option lets you add document types that should be considered as downloads. Separate them with comma.', 'gital-gs' ) . '</i></p>';
		}

		/**
		 * Renders the "Activate Google Analytics Consent Mode" checkbox
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.5.0
		 */
		public function settings_activate_ga_consent_mode_render() {
			echo '<input type="checkbox" name="g_gs_settings[activate_ga_consent_mode]" value="1"' . checked( 1, $this->options['activate_ga_consent_mode'] ?? '', false ) . '>';
			echo '<p><i>' . __( 'This option will activate the Google Analytics Consent Mode', 'gital-gs' ) . '</i></p>';
		}

		/**
		 * Renders the "Google Analytics ID" field
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.2.0
		 */
		public function settings_ga_id_render() {
			echo '<input type="text" name="g_gs_settings[ga_id]" value="' . esc_js( $this->options['ga_id'] ?? '' ) . '">';
		}

		/**
		 * Renders the "Second gtm ID" field
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.2.0
		 */
		public function settings_second_gtag_id_render() {
			echo '<input type="text" name="g_gs_settings[second_gtag_id]" value="' . esc_js( $this->options['second_gtag_id'] ?? '' ) . '">';
		}

		/**
		 * Renders the "Additional script" field
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.2.0
		 */
		public function settings_additional_script_render() {
			$user = wp_get_current_user()->user_login;
			if ( 'gibonadmin' === $user ) {
				echo '<textarea name="g_gs_settings[additional_script]" rows="3" cols="80">' . esc_html( $this->options['additional_script'] ?? '' ) . '</textarea>';
			} else {
				echo '<textarea disabled rows="3" cols="80">' . __( 'Disabled for security reasons', 'gital-gs' ) . '</textarea>';
			}
		}

		/**
		 * Renders the "Google Tag Manager ID" field
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.2.0
		 */
		public function settings_gtm_id_render() {
			echo '<input type="text" name="g_gs_settings[gtm_id]" value="' . esc_js( $this->options['gtm_id'] ?? '' ) . '">';
		}

		/**
		 * Define the settings and the fields
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.2.0
		 */
		public function settings_init() {
			// Define the settings.
			register_setting( 'g_gs', 'g_gs_settings' );

			// Define the sections.
			add_settings_section(
				'g_gs_settings_ga_section',
				__( 'Google Analytics', 'gital-gs' ),
				array( $this, 'settings_section_ga_description' ),
				'g_gs'
			);

			add_settings_section(
				'g_gs_settings_gtm_section',
				__( 'Google Tag Manager', 'gital-gs' ),
				array( $this, 'settings_section_gtm_description' ),
				'g_gs'
			);

			// Define the fields.
			add_settings_field(
				'activate_ga',
				__( 'Activate Google Analytics', 'gital-gs' ),
				array( $this, 'settings_activate_ga_render' ),
				'g_gs',
				'g_gs_settings_ga_section'
			);

			add_settings_field(
				'activate_ga_consent_mode',
				__( 'Activate Google Analytics Consent Mode', 'gital-gs' ),
				array( $this, 'settings_activate_ga_consent_mode_render' ),
				'g_gs',
				'g_gs_settings_ga_section'
			);

			add_settings_field(
				'activate_form_tracking',
				__( 'Activate Form Tracking', 'gital-gs' ),
				array( $this, 'settings_activate_form_tracking_render' ),
				'g_gs',
				'g_gs_settings_ga_section'
			);

			add_settings_field(
				'activate_phone_tracking',
				__( 'Activate Phone Tracking', 'gital-gs' ),
				array( $this, 'settings_activate_phone_tracking_render' ),
				'g_gs',
				'g_gs_settings_ga_section'
			);

			add_settings_field(
				'activate_mail_tracking',
				__( 'Activate E-Mail Tracking', 'gital-gs' ),
				array( $this, 'settings_activate_mail_tracking_render' ),
				'g_gs',
				'g_gs_settings_ga_section'
			);

			add_settings_field(
				'activate_film_tracking',
				__( 'Activate Film Tracking', 'gital-gs' ),
				array( $this, 'settings_activate_film_tracking_render' ),
				'g_gs',
				'g_gs_settings_ga_section'
			);

			add_settings_field(
				'activate_downloads_tracking',
				__( 'Activate Downloads Tracking', 'gital-gs' ),
				array( $this, 'settings_activate_downloads_tracking_render' ),
				'g_gs',
				'g_gs_settings_ga_section'
			);

			add_settings_field(
				'downloads_tracking_documents',
				__( 'Documents to track', 'gital-gs' ),
				array( $this, 'settings_downloads_tracking_documents_render' ),
				'g_gs',
				'g_gs_settings_ga_section'
			);

			add_settings_field(
				'ga_id',
				__( 'Google Analytics ID', 'gital-gs' ),
				array( $this, 'settings_ga_id_render' ),
				'g_gs',
				'g_gs_settings_ga_section'
			);

			add_settings_field(
				'second_gtag_id',
				__( 'Second  gtag ID', 'gital-gs' ),
				array( $this, 'settings_second_gtag_id_render' ),
				'g_gs',
				'g_gs_settings_ga_section'
			);

			add_settings_field(
				'additional_script',
				__( 'Additional script', 'gital-gs' ),
				array( $this, 'settings_additional_script_render' ),
				'g_gs',
				'g_gs_settings_ga_section'
			);

			add_settings_field(
				'activate_gtm',
				__( 'Activate Google Tag Manager', 'gital-gs' ),
				array( $this, 'settings_activate_gtm_render' ),
				'g_gs',
				'g_gs_settings_gtm_section'
			);

			add_settings_field(
				'gtm_id',
				__( 'Google Tag Manager ID', 'gital-gs' ),
				array( $this, 'settings_gtm_id_render' ),
				'g_gs',
				'g_gs_settings_gtm_section'
			);
		}
	}
}
