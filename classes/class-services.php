<?php
/**
 * Services
 *
 * @package Gital Google Services
 */

namespace gital_google_services;

if ( ! class_exists( 'Services' ) ) {
	/**
	 * Services
	 *
	 * Initializes the services
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 * @version 1.5.0
	 * @since 1.2.0
	 */
	class Services {
		/**
		 * The options
		 *
		 * @var array
		 */
		public $options;

		public function __construct() {
			if ( is_admin() ) {
				return;
			}

			$this->options = get_option( 'g_gs_settings' );

			if ( isset( $this->options['activate_ga'] ) && $this->options['activate_ga'] && isset( $this->options['ga_id'] ) && $this->options['ga_id'] ) {
				add_action( 'wp_enqueue_scripts', array( $this, 'ga_header' ), 30 );
			}

			if ( isset( $this->options['activate_gtm'] ) && $this->options['activate_gtm'] && isset( $this->options['gtm_id'] ) && $this->options['gtm_id'] ) {
				add_action( 'wp_enqueue_scripts', array( $this, 'gtm_header' ), 30 );
				add_action( 'theme_after_body_tag_start', array( $this, 'gtm_body' ) );
			}

			if ( isset( $this->options['activate_form_tracking'] ) && $this->options['activate_form_tracking'] ) {
				add_action( 'wp_enqueue_scripts', array( $this, 'form_tracker' ), 40 );
			}

			if ( isset( $this->options['activate_phone_tracking'] ) && $this->options['activate_phone_tracking'] ) {
				add_action( 'wp_enqueue_scripts', array( $this, 'phone_tracker' ), 40 );
			}

			if ( isset( $this->options['activate_mail_tracking'] ) && $this->options['activate_mail_tracking'] ) {
				add_action( 'wp_enqueue_scripts', array( $this, 'mail_tracker' ), 40 );
			}

			if ( isset( $this->options['activate_film_tracking'] ) && $this->options['activate_film_tracking'] ) {
				add_action( 'wp_enqueue_scripts', array( $this, 'film_tracker' ), 40 );
			}

			if ( isset( $this->options['activate_downloads_tracking'] ) && $this->options['activate_downloads_tracking'] ) {
				add_action( 'wp_enqueue_scripts', array( $this, 'downloads_tracker' ), 40 );
			}
		}

		/**
		 * Renders the Google Analytics GTAG meant for the header
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.2.0
		 * @since 1.2.0
		 */
		public function ga_header() {
			wp_register_script( 'g_gs_google_analytics', 'https://www.googletagmanager.com/gtag/js?id=' . esc_js( $this->options['ga_id'] ), array(), '1.0.0', false );
			wp_enqueue_script( 'g_gs_google_analytics' );

			// Defining the inline Google Analytics script.
			$inline_ga_script_before = 'window.dataLayer = window.dataLayer || []; function gtag() { dataLayer.push(arguments); }';
			wp_add_inline_script( 'g_gs_google_analytics', $inline_ga_script_before, 'before' );
			$inline_ga_script_after = "gtag('js', new Date()); gtag('config', '" . esc_js( $this->options['ga_id'] ) . "');";
			wp_add_inline_script( 'g_gs_google_analytics', $inline_ga_script_after, 'after' );

			// Adding Google Analytics Consent Mode.
			if ( isset( $this->options['activate_ga_consent_mode'] ) && $this->options['activate_ga_consent_mode'] ) {
				$inline_second_gtag_id = "gtag('consent','default',{'security_storage':'granted','functionality_storage':'denied','personalization_storage':'denied','ad_storage':'denied','ad_user_data':'denied','ad_personalization':'denied','analytics_storage':'denied','wait_for_update':500});";
				wp_add_inline_script( 'g_gs_google_analytics', $inline_second_gtag_id, 'before' );
			}

			// Adding the second gtag.
			if ( isset( $this->options['second_gtag_id'] ) && $this->options['second_gtag_id'] ) {
				$inline_second_gtag_id = "gtag('config', '" . esc_js( $this->options['second_gtag_id'] ) . "');";
				wp_add_inline_script( 'g_gs_google_analytics', $inline_second_gtag_id, 'after' );
			}

			// Adding the additional script.
			if ( isset( $this->options['additional_script'] ) && $this->options['additional_script'] ) {
				$additional_script = $this->options['additional_script'];
				wp_add_inline_script( 'g_gs_google_analytics', $additional_script, 'after' );
			}
		}

		/**
		 * Renders the Google Tag Manager script meant for the header
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 * @since 1.2.0
		 */
		public function gtm_header() {
			wp_register_script( 'g_gs_gtm', false, array(), '1.0.0', false );
			wp_enqueue_script( 'g_gs_gtm' );

			// Defining the inline Google Tag Manager script.
			$inline_gtm_script = "(function(w, d, s, l, i) { w[l] = w[l] || []; w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' }); var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f); })(window, document, 'script', 'dataLayer', '" . esc_js( $this->options['gtm_id'] ) . "');";
			wp_add_inline_script( 'g_gs_gtm', $inline_gtm_script, 'after' );
		}

		/**
		 * Renders the Google Tag Manager script meant for the body
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.2.0
		 */
		public function gtm_body() {
			?>
			<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo esc_js( $this->options['gtm_id'] ); ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
			<?php
		}

		/**
		 * Renders the Form Tracking Script for Ninja Forms meant for the footer
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.1
		 * @since 1.3.0
		 */
		public function form_tracker() {
			wp_register_script( 'g_gs_form_tracker', false, array(), '1.0.0', true );
			wp_enqueue_script( 'g_gs_form_tracker' );

			// Defining the inline form tracker script.
			$inline_form_tracker_script = "jQuery(document).on('nfFormReady', function() { nfRadio.channel('forms').on('submit:response', function(form) { gtag('event', 'Submit', { 'event_category': 'Form', 'event_label': form.data.settings.title }); console.log(form.data.settings.title + ' successfully submitted'); }); });";
			wp_add_inline_script( 'g_gs_form_tracker', $inline_form_tracker_script, 'after' );
		}

		/**
		 * Renders the Phone Tracking Script
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.2.0
		 * @since 2.0.0
		 */
		public function phone_tracker() {
			wp_register_script( 'g_gs_phone_tracker', false, array(), '1.0.0', true );
			wp_enqueue_script( 'g_gs_phone_tracker' );

			// Defining the inline form tracker script.
			$inline_phone_tracker_script = "window.addEventListener('load',()=>{const phone_links=document.querySelectorAll(" . '"' . "a[href^='tel']" . '"' . ");if(phone_links){phone_links.forEach((phone_link)=>{phone_link.addEventListener('click',()=>{gtag('event','Call',{event_category:'Phone',event_label:phone_link.getAttribute('href')})})})}})";
			wp_add_inline_script( 'g_gs_phone_tracker', $inline_phone_tracker_script, 'after' );
		}

		/**
		 * Renders the Mail Tracking Script
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 2.0.0
		 */
		public function mail_tracker() {
			wp_register_script( 'g_gs_mail_tracker', false, array(), '1.0.0', true );
			wp_enqueue_script( 'g_gs_mail_tracker' );

			// Defining the inline form tracker script.
			$inline_mail_tracker_script = "window.addEventListener('load',()=>{const mail_links=document.querySelectorAll(" . '"' . "a[href^='mailto']" . '"' . ");if(mail_links){mail_links.forEach((mail_link)=>{mail_link.addEventListener('click',()=>{gtag('event','Mail',{event_category:'Mail',event_label:mail_link.getAttribute('href')})})})}})";
			wp_add_inline_script( 'g_gs_mail_tracker', $inline_mail_tracker_script, 'after' );
		}

		/**
		 * Renders the Film Tracking Script
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 2.0.0
		 */
		public function film_tracker() {
			wp_register_script( 'g_gs_film_tracker', false, array(), '1.0.0', true );
			wp_enqueue_script( 'g_gs_film_tracker' );

			// Defining the inline film tracker script.
			$inline_film_tracker_script = "window.addEventListener('load',()=>{const film_elements=document.querySelectorAll('video');if(film_elements){film_elements.forEach((film_element)=>{if(film_element.hasAttribute('autoplay')){return}film_element.addEventListener('play',()=>{gtag('event','Film Played',{event_category:'Film',event_label:film_element.currentSrc,})})})}})";
			wp_add_inline_script( 'g_gs_film_tracker', $inline_film_tracker_script, 'after' );
		}

		/**
		 * Renders the Downloads Tracking Script
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 2.0.0
		 */
		public function downloads_tracker() {
			wp_register_script( 'g_gs_downloads_tracker', false, array(), '1.0.0', true );
			wp_enqueue_script( 'g_gs_downloads_tracker' );

			// Defining the inline download documents array.
			if ( isset( $this->options['downloads_tracking_documents'] ) && $this->options['downloads_tracking_documents'] ) {
				$document_types_to_track                    = explode( ',', esc_html( $this->options['downloads_tracking_documents'] ) );
				$inline_downloads_tracking_documents_script = 'const g_gs_downloads_tracking_documents=' . wp_json_encode( $document_types_to_track ) . ';';
				wp_add_inline_script( 'g_gs_downloads_tracker', $inline_downloads_tracking_documents_script, 'before' );
			}

			// Defining the inline downloads tracker script.
			$inline_downloads_tracker_script = "window.addEventListener('load',()=>{let download_links=document.querySelectorAll('a[download]');if(g_gs_downloads_tracking_documents){g_gs_downloads_tracking_documents.forEach(file_ending=>{const additional_document_links=document.querySelectorAll('[href$=" . '".' . "'+file_ending+'" . '"' . " i]');if(additional_document_links){additional_document_links.forEach(additional_document_link=>{download_links=[...download_links,additional_document_link]})}})};download_links = [...new Set(download_links)];if(download_links){download_links.forEach((download_link)=>{download_link.addEventListener('click',()=>{gtag('event','Download',{event_category:'Download',event_label:download_link.getAttribute('href')})})})}})";
			wp_add_inline_script( 'g_gs_downloads_tracker', $inline_downloads_tracker_script, 'after' );
		}
	}
}
