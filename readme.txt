=== Gital Google Services ===
Contributors: gibonadmin
Requires at least: 5.0
Tested up to: 5.5
Requires PHP: 7.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The Gital Google Services is made with passion in Uppsala, Sweden. The plugin is adding Google Services such as Google Analytics and Google Tag Manager to your site. If you'd like support, please contact us at webb@gibon.se.

== Description ==
The Gital Google Services is made with passion in Uppsala, Sweden. The plugin is adding Google Services such as Google Analytics and Google Tag Manager to your site. If you'd like support, please contact us at webb@gibon.se.

== Installation ==
Add the plugin and edit the settings from the settings menu.

== Changelog ==

= 2.2.0 - 2024.09.04 =
* The plugin now utilizes the new repo.

= 2.1.0 =
* Update: The Cookie Consent mode script is updated.

= 2.0.1 =
* Bugfix: The Form tracker is now using jQuery again.

= 2.0.0 =
* Update: Added tracking of mailto: links
* Update: Added tracking of downloads
* Update: Added tracking of film playback
* Update: Moved from jQuery to Vanilla JS
* Bugfix: The GTM script did not have a version

= 1.6.0 =
* Update: Gibon Webb Uppsala is now Gibon Webb

= 1.5.1 =
* Update: Added Google Analytics Consent Mode

= 1.4.1 =
* Update: Changed the order of the hooks to load the scripts later

= 1.4.0 =
* Update: Updated to use native Wordpress functions for registering scripts

= 1.3.2 =
* Bugfix: Just a cleanup on the call event

= 1.3.1 =
* Bugfix: The Phone script did not run on some pages due to late loading of jQuery

= 1.2.5 =
* Bugfix: The GTM ID did not render

= 1.2.4 =
* Bugfix: The GTAG ID did not render

= 1.2.2 =
* Update: Cleaned up even more and moved functions to classes

= 1.0.0 =
* Update: Cleaned up the plugin and improved the documentation

= 0.2.2 =
* Update: Moved plugin-update-checker to composer

= 0.1.7 =
* add_action('plugins_loaded'...) instead of add_action('init'...)

= 0.1.6 =
* Bugfix with !array_key_exists(). Added is_array() && before.

= 0.1.5 =
* Added composer support

= 0.1.4 =
* Major Bugfix

= 0.1.3 =
* Bugfixes - The ID for GTM was wrong

= 0.1.2 =
* Bugfixes

= 0.1.1 =
* Bugfixes

= 0.1.0 =
* New variables

= 0.0.10 =
* New labels

= 0.0.9 =
* New labels

= 0.0.8 =
* New sections

= 0.0.7 =
* Improved translations

= 0.0.6 =
* Test

= 0.0.5 =
* Added updater and moved to wp settings

= 0.0.4 =
* Added internationalization

= 0.0.3 =
* Added activation toggles and removed config variable

= 0.0.2 =
* Switched to GTAG
* Added event tracking
* Moved IDs to fields

= 0.0.1 =
* Init