<?php
/**
 * Plugin Name: Gital Google Services
 * Author: Gibon Webb
 * Version: 2.2.0
 * Text Domain: gital-gs
 * Domain Path: /languages/
 *
 * Author URI: https://gibon.se/
 * Description: The Gital Google Services is made with passion in Uppsala, Sweden. The plugin is adding Google Services such as Google Analytics and Google Tag Manager to your site. If you'd like support, please contact us at webb@gibon.se.
 *
 * @package Gital Google Services
 */

namespace gital_google_services;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Loads text domain
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 * @since 0.0.4
 */
function load_textdomain() {
	load_plugin_textdomain( 'gital-gs', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'gital_google_services\load_textdomain', 10 );

// Define constants.
if ( ! defined( 'G_GS_ROOT' ) ) {
	define( 'G_GS_ROOT', plugins_url( '', __FILE__ ) );
}
if ( ! defined( 'G_GS_ROOT_PATH' ) ) {
	define( 'G_GS_ROOT_PATH', plugin_dir_path( __FILE__ ) );
}
if ( ! defined( 'G_GS_CLASSES_PATH' ) ) {
	define( 'G_GS_CLASSES_PATH', G_GS_ROOT_PATH . 'classes/' );
}
if ( ! defined( 'G_GS_VENDOR_PATH' ) ) {
	define( 'G_GS_VENDOR_PATH', G_GS_ROOT_PATH . 'vendor/' );
}

// Init the updater.
require G_GS_VENDOR_PATH . 'yahnis-elsts/plugin-update-checker/plugin-update-checker.php';
use \YahnisElsts\PluginUpdateChecker\v5\PucFactory;
$myUpdateChecker = PucFactory::buildUpdateChecker(
	'https://packages.gital.se/wordpress/gital-google-services.json',
	__FILE__,
	'gital-google-services'
);

// Require and init classes.
require_once G_GS_CLASSES_PATH . 'class-option-page.php';
new Option_Page();

require_once G_GS_CLASSES_PATH . 'class-services.php';
new Services();
